# Use

This is a simple application that uses e-mail clients found on Windows/Linux/Mac to create an e-mail with attachments, at the moment this application has fixed tests which are easily replaced and are working with Thunderbird and Outlook on Windows.

On Windows it uses the registry to find the default e-mail client and tries to create the e-mail by using their CLI parameters.  This could be changed (but wouldn't work 100%) by creating an eml file and opening it in any e-mail client (but some e-mail clients will open it as read-only).

It can be easily expanded to fetch any e-mail clients

# Next Steps

- Use arguments to fetch the required data for the e-mail
- Detach from process to avoid keeping CLI open
- Search and find the proper e-mail clients in Linux and MacOS (most likely Thunderbird)
- Expand windows to use windows live mail? (not sure this is possible at the moment)

