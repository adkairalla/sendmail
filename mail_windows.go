package leMailSender

import (
	"fmt"
	"log"
	"os/exec"
	"strings"

	"golang.org/x/sys/windows/registry"
)

func SendMail() {
	var cmd *exec.Cmd

	k, err := registry.OpenKey(registry.LOCAL_MACHINE, `SOFTWARE\Clients\Mail`, registry.QUERY_VALUE)
	if err != nil {
		log.Fatal("Finding Key: ", err)
	}

	mailClient, _, err := k.GetStringValue("")
	if err != nil {
		log.Fatal("Finding String Value: ", err)
	}

	fmt.Printf("Key Value: %+v\nString Value: %+v\n", k, mailClient)

	exePath := assembleMailClientPath(mailClient)

	switch {
	case mailClient == "Mozilla Thunderbird":
		fmt.Println("Hmm...")
		cmd = exec.Command(exePath, "-compose", "to=piko@piko.com,subject=hello,attachment=C:\\Users\\Piko\\Downloads\\teste.bb")
		break
	case mailClient == "Microsoft Outlook":
		fmt.Println("Hmm Outlook...")
		cmd = exec.Command(exePath, "/a", "teste.bb", "/m", "piko@piko.com?subject=hello")
		break
	default:
		fmt.Println("No compatible mail clients found")
		return
	}

	cmd.Run()
}

func assembleMailClientPath(client string) string {
	pathRegistry := `SOFTWARE\Clients\Mail\` + client + `\shell\open\command`

	pathKey, err := registry.OpenKey(registry.LOCAL_MACHINE, pathRegistry, registry.QUERY_VALUE)
	if err != nil {
		return ""
	}

	pathString, _, err := pathKey.GetStringValue("")
	if err != nil {
		return ""
	}

	pathSlice := strings.Split(pathString, "\"")

	return pathSlice[1]
}
